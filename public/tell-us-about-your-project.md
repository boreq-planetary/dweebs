# Tell Us About Your project

5mins of people talking about something they give a shit about

---

## Planetary SSB Client - Matt Lorentz

<iframe title="Planetary Reverse Conf" width="560" height="315" src="https://tube.tchncs.de/videos/embed/4af7eb28-9643-45ab-a3a7-4f6720a98c1a" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Imagining Heavens and Hells for Decentralized Archiving on Mobile

...In which we each tell each other something about archiving mobile media to decentralized storage

<iframe title="Imagining Heavens and Hells for Decentralized Archiving on Mobile" width="560" height="315" src="https://www.youtube.com/embed/8VEHNioiFAg" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

-[John Hess](mailto:john@jthess.com) of [Open Archive](https://open-archive.org)

## Mix

<iframe width="560" height="315" src="https://www.youtube.com/embed/SNTcR6t97eE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Likes**:
p2p,
growing community,
sci-fi,
magic

**Links**:
[ahau.io](https://ahau.io),
[scuttlebutt.nz](https://www.scuttlebutt.nz),
[planetary.social](https://www.planetary.social),
[smat-app.com](https://www.smat-app.com)


## Ian Tairea - Project Sunrise

<iframe width="560" height="315" src="https://www.youtube.com/embed/JMeO9JdakqY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


