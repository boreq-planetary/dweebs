
# Longer talks

---

## Mix Irving - FOSDEM2022

<iframe width="560" height="315" src="https://www.youtube.com/embed/g2yE9t0d7ac" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

data sovereignty, indegenous identitiy

## Hannah Howard - Strange Loop 2019

<iframe width="560" height="315" src="https://www.youtube.com/embed/QZbzD2Wc_dQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

how we tell stories in code

## Jay Carpenter - Decentralized Voting & Zero-Knowledge Proofs

<iframe width="560" height="315" src="https://www.youtube.com/embed/FxqK0riZDQw?start=569" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Slides](https://docs.google.com/presentation/d/1LxFysYro43HLug77iCW2f129oY9mSSCi7MYyKkCy7u8/edit?usp=sharing)<br />
A more personal, [shorter video](https://www.youtube.com/watch?v=reQ71E_E1pQ)

## Guardian Project - "We're going to camp... off-grid privacy tech ftw!"

<iframe width="560" height="315" src="https://www.youtube.com/embed/tZJJh1ZxdIM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Alexander Cobleigh — TrustNet: Subjective Trust-based Moderation Systems
<iframe width="560" height="315" src="https://www.youtube.com/embed/bVkesgWZlTU" title="TrustNet - Master's Thesis Presentation (Alexander Cobleigh)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

TrusNet is a building block for subjective trust-based moderation systems, specifically tailored for p2p distributed chat systems.

Also available in [blog form](http://cblgh.org/trustnet) (links to repositories inside!)
